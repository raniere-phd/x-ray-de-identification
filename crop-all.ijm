/*
ImageJ/Fiji macro for Radiograph (aka X-ray) de-identification

This macro will crop the radiograph to remove burn information
*/

var number_of_lines = 3;
var line_height = 30;
var good_roi = false;
var is_label_at_top = true;

/*
headless_arg = getArgument();

if(lengthOf(headless_arg) == 0) {
    run("Quit");
}

if(headless_arg.endsWith("/")) {
    root_path = headless_arg;
}
else {
    root_path = headless_arg + "/";
}

*/
root_path = '/home/raniere/hkjc/';

input_path = root_path + 'mark/';

output_path = root_path + 'mark_removed/';

zip_files = getFileList(input_path);

for(i=0; i<zip_files.length; i++) {
    print('Processing ' + input_path + zip_files[i]);

    working_path = output_path + replace(zip_files[i], '.zip', '') + '/';

    // print('Unzip to ' + working_path);
    // exec('mkdir -p ' + working_path);
    // exec(
    // 	'unzip ' + input_path + zip_files[i] + ' -d ' + working_path
	    
    // );

    dicom_files =  getFileList(working_path);

    for(j=0; j<dicom_files.length; j++) {
	if(endsWith(dicom_files[j], '.dcm')) {

	    print('Processing ' + working_path + dicom_files[j]);
	    open(working_path + dicom_files[j]);

	    do {
		
		if (is_label_at_top) {
		    top_cut = number_of_lines * line_height;  /* px */
		    bottom_cut = 0;  /* px */
		    makeRectangle(0, top_cut, getWidth(), getHeight() - top_cut - bottom_cut);
		}
		else {
		    top_cut = 0;  /* px */
		    bottom_cut = number_of_lines * line_height;  /* px */
		    makeRectangle(0, 0, getWidth(), getHeight() - top_cut - bottom_cut);
		}

		Dialog.create("X-ray De-identification");

		Dialog.addCheckbox('Is the ROI good?', true);
		
		Dialog.show();

		good_roi = Dialog.getCheckbox();

		if(good_roi) {
		    run("Crop");

		    saveAs(
			"Tiff",
			working_path + replace(dicom_files[j], '.dcm', '.tiff')
		    );

		}
		else {
		    Dialog.create("X-ray De-identification");

		    Dialog.addCheckbox("Label at Top left", true);
		    Dialog.addNumber("Number of lines to remove:", number_of_lines);
		    Dialog.addNumber("Individual Line Height (px):", line_height);
		    
		    Dialog.show();

		    is_label_at_top = Dialog.getCheckbox();
		    number_of_lines = Dialog.getNumber();
		    line_height = Dialog.getNumber();
		}

	    } while(!good_roi);

	    close();
	}

    }

}





