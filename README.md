# X-ray De-identification

Radiograph (aka X-ray) de-identification
using
[Fiji](https://fiji.sc/).

## Pre-requirements

- Java, for example [OpenJDK](https://openjdk.java.net/)
- Fiji

## Run

### Cover All Burn-in Patient Information

```
$ fiji -macro cover-info.ijm /path/to/radiographs/
```

### Cover All Burn-in Text

```
$ fiji -macro cover-all.ijm /path/to/radiographs/
```

### Crop All Burn-in Text

```
$ fiji -macro crop-all.ijm /path/to/radiographs/
```

## Frequently Asked Questions (FAQ)

1. Why use Fiji?

   Fiji has support to script.
1. Why save the output as Tiff?

   Because Tiff preserves the metadata
   and
   supports a wide range of bits stored values.
   PNG does not have this features.
1. If I want to read the Tiff files in Python,
   what package should I use?

   tifffile. https://github.com/cgohlke/tifffile