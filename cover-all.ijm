/*
ImageJ/Fiji macro

This macro will prompt the user to regions to remove from radiographs.
*/

function open_and_cover() {
    setForegroundColor(0, 0, 0);

    open(radiograph_path);
    
    if(top_left) {
	makeRectangle(
	    0,
	    0,
	    30 * line_height * 0.8,
	    line_height * top_left_lines
	);
	run("Fill", "slice");
    }

    if(top_right) {
	makeRectangle(
	    getWidth() - 30 * line_height * 0.8,
	    0,
	    30 * line_height * 0.8,
	    line_height * top_left_lines
	);
	run("Fill", "slice");
    }

    if(bottom_left) {
	if(is_bottom_left_meta) {
	    makeRectangle(
		0,
		getHeight() - line_height * 2,
		12 * line_height * 0.8,
		line_height * 2
	    );
	    run("Fill", "slice");	    
	}
	else {
	    makeRectangle(
		0,
		getHeight() - line_height * top_left_lines,
		30 * line_height * 0.8,
		line_height * top_left_lines
	    );
	    run("Fill", "slice");
	}
    }

    if(bottom_right) {
	if(is_bottom_right_meta) {
	    makeRectangle(
		getWidth() - 8 * line_height * 0.8,
		getHeight() - line_height * 1,
		8 * line_height * 0.8,
		line_height * 1
	    );
	    run("Fill", "slice");
	}
	else {
	    makeRectangle(
		getWidth() - 30 * line_height * 0.8,
		getHeight() - line_height * top_left_lines,
		30 * line_height * 0.8,
		line_height * top_left_lines
	    );
	    run("Fill", "slice");
	}
    }
}

headless_arg = getArgument();

if(headless_arg.length == 0) {
    root_path = getDirectory("Choose root directory");
}
else {
    if(headless_arg.endsWith("/")) {
        root_path = headless_arg;
    }
    else {
        root_path = headless_arg + "/";
    }
}

source_path = root_path + "mark/";
destination_path = root_path + "clean/";

log_path = root_path + "cover-all.log.csv";
log_file = File.open(log_path);

print("Root path: " + root_path);
print("Source path: " + source_path);
print("Destination path: " + destination_path);
print("Log path: " + log_path);

radiograph_set_ids = getFileList(source_path);

top_left = true;
top_right = false;
bottom_left = false;
is_bottom_left_meta = false;
bottom_right = false;
is_bottom_right_meta = false;
line_height = 30;
top_left_lines = 6;
radiograph_is_cover = false;

for(i=0; i<radiograph_set_ids.length; i++) {
//for(i=0; i<1; i++) {
    print(i + ": " + radiograph_set_ids[i]);
    radiograph_set_path = source_path + radiograph_set_ids[i];
    
    radiograph_set_output_path = destination_path + radiograph_set_ids[i];	    
    File.makeDirectory(radiograph_set_output_path);

    radiographs = getFileList(radiograph_set_path);
    for(j=0; j<radiographs.length; j++) {
    //for(j=0; j<1; j++) {
	print(j + ": " + radiographs[j]);
	radiograph_path = radiograph_set_path + radiographs[j];
	radiograph_output_path = radiograph_set_output_path + radiographs[j];

	while(!radiograph_is_cover){
	    open_and_cover();
	
	    Dialog.create("My inputs");
	    Dialog.addCheckbox("Cover", radiograph_is_cover);
	    Dialog.addNumber("Line height", line_height);
	    Dialog.addCheckbox("Top left", top_left);
	    Dialog.addNumber("Top left lines", top_left_lines);
	    Dialog.addCheckbox("Top right", top_right);
	    Dialog.addCheckbox("Bottom left", bottom_left);
	    Dialog.addCheckbox("Is bottom left meta", is_bottom_left_meta);
	    Dialog.addCheckbox("Bottom right", bottom_right);
	    Dialog.addCheckbox("Is bottom right meta", is_bottom_right_meta);
	    
	    Dialog.show();

	    radiograph_is_cover = Dialog.getCheckbox();
	    line_height = Dialog.getNumber(); 
	    top_left = Dialog.getCheckbox();
	    top_left_lines = Dialog.getNumber(); 
	    top_right = Dialog.getCheckbox();
	    bottom_left = Dialog.getCheckbox();
	    is_bottom_left_meta = Dialog.getCheckbox();
	    bottom_right = Dialog.getCheckbox();
	    is_bottom_right_meta = Dialog.getCheckbox();

	    if(radiograph_is_cover){
		print(
		    log_file,
		    radiograph_path + "," +
			radiograph_output_path + "," +
			line_height + "," +
			top_left + "," +
			top_left_lines + "," +
			top_right + "," +
			bottom_left + "," +
			is_bottom_left_meta + "," +
			bottom_right + "," +
			is_bottom_right_meta
		);
		saveAs(
		    "Tiff",
		    radiograph_output_path
		);
	    }
	    close();
	}
	radiograph_is_cover = false;
    }
}

File.close(log_file);
