/*
ImageJ/Fiji macro for Radiograph (aka X-ray) de-identification

This macro will crop the radiograph to remove burn information
*/

var number_of_lines = 3;
var line_height = 30;
var width = 500;
var vertical_padding = 0;
var good_roi = false;
var is_label_at_top = true;
var is_label_at_left = true;

/*
headless_arg = getArgument();

if(lengthOf(headless_arg) == 0) {
    run("Quit");
}

if(headless_arg.endsWith("/")) {
    root_path = headless_arg;
}
else {
    root_path = headless_arg + "/";
}

*/
root_path = '/home/raniere/hkjc/';

input_path = root_path + 'mark/';

working_path = root_path + 'mark_working/';

output_path = root_path + 'mark_removed/';

zip_files = getFileList(input_path);

for(i=0; i<zip_files.length; i++) {
    print('Processing ' + input_path + zip_files[i]);

    zip_working_path = working_path + replace(zip_files[i], '.zip', '') + '/';
    zip_output_path = output_path + replace(zip_files[i], '.zip', '') + '/';

    exec('mkdir -p ' + zip_working_path);
    exec('mkdir -p ' + zip_output_path);

    print('Unzip to ' + zip_working_path);
    exec(
	'unzip ' + input_path + zip_files[i] + ' -d ' + zip_working_path
	    
    );

    dicom_files =  getFileList(zip_working_path);

    for(j=0; j<dicom_files.length; j++) {
	if(endsWith(dicom_files[j], '.dcm')) {

	    print('Processing ' + zip_working_path + dicom_files[j]);
	    open(zip_working_path + dicom_files[j]);

	    do {
		
		height = number_of_lines * line_height;  /* px */
		if (is_label_at_top && is_label_at_left) {
		    makeRectangle(0, 0, width, height);
		}
		if (is_label_at_top && !is_label_at_left) {
		    makeRectangle(0, getWidth() - width, width, height);
		}
		if (!is_label_at_top && is_label_at_left) {
		    makeRectangle(0, getHeight() - height - vertical_padding, width, height);
		}
		if (!is_label_at_top && !is_label_at_left) {
		    makeRectangle(0, getWidth() - width, width, height);
		}

		Dialog.create("X-ray De-identification");

		Dialog.addCheckbox('Is the ROI good?', true);
		
		Dialog.show();

		good_roi = Dialog.getCheckbox();

		if(good_roi) {
		    setForegroundColor(0, 0, 0);
		    run("Fill", "slice");

		    saveAs(
			"Tiff",
			zip_output_path + replace(dicom_files[j], '.dcm', '.tiff')
		    );

		}
		else {
		    Dialog.create("X-ray De-identification");

		    Dialog.addCheckbox("Label at Top", is_label_at_top);
		    Dialog.addCheckbox("Label at Left", is_label_at_left);
		    Dialog.addNumber("Number of lines to remove:", number_of_lines);
		    Dialog.addNumber("Individual Line Height (px):", line_height);
		    Dialog.addNumber("Vertical padding (px):", vertical_padding);
		    Dialog.addNumber("Width (px):", width);
		    
		    Dialog.show();

		    is_label_at_top = Dialog.getCheckbox();
		    is_label_at_left = Dialog.getCheckbox();
		    number_of_lines = Dialog.getNumber();
		    line_height = Dialog.getNumber();
		    vertical_padding = Dialog.getNumber();
		    width = Dialog.getNumber();
		}

	    } while(!good_roi);

	    close();
	}

    }

}

print('No more files to process.');
